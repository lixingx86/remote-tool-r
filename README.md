# RemoteTool

#### 介绍
一个远程辅助工具，目前实现了远程设备信息的记录、搜索、一键连接的功能，支持国内的ToDesk和向日葵的远程连接，从此妈妈再也不用担心我记不住账号了

#### 软件架构
软件架构说明
MvvmLight + HandyControl + Sqlite + SqlSugar
使用了HandyControl的例程模板进行了改造，简单化了界面

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  首先配置ToDesk和向日葵的程序执行路径!
[输入图片说明](https://foruda.gitee.com/images/1659893682452296442/qq图片20220808013106.png "QQ图片20220808013106.png")

2.  新建设备，添加设备信息后保存，就可以在主界面看到设备信息，按空格键会查询最近10个使用过的设备信息
![输入图片说明](https://foruda.gitee.com/images/1659893725095460751/qq图片20220808013322.png "QQ图片20220808013322.png")

3.  输入搜索的关键词，点击搜索或者敲击键盘enter键进行搜索，可以使用项目名称、设备名称、项目名称+空格+设备名称组合进行关键词索引
![输入图片说明](https://foruda.gitee.com/images/1659893762063452425/qq图片20220808013253.png "QQ图片20220808013253.png")

4.  ToDesk采用了自带支持命令行参数调用的方式远程账号，向日葵10版本以后不支持这种方式，超过10版本后的向日葵采用了模拟键盘输入的
    方式进行远程连接，所以会比较慢一些

可以点击右上角的配置图标，修改主题
![输入图片说明](https://foruda.gitee.com/images/1659893854136452135/qq图片20220808013008.png "QQ图片20220808013008.png")
![输入图片说明](https://foruda.gitee.com/images/1659893867518511491/qq图片20220808013149.png "QQ图片20220808013149.png")
![输入图片说明](https://foruda.gitee.com/images/1659893884296517043/qq图片20220808013208.png "QQ图片20220808013208.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
