﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.Common
{
    public static class CommonHelper
    {
        public static T CopyObjValue<T>(this T toobj, Object fromobj) where T : class
        {
            if (fromobj != null && toobj != null)
            {
                var otherobjPorps = fromobj.GetType().GetProperties();
                foreach (var formp in otherobjPorps)
                {
                    var top = toobj.GetType().GetProperty(formp.Name);
                    if (top != null)
                    {
                        try
                        {
                            top.SetValue(toobj, formp.GetValue(fromobj));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
            }
            return toobj;
        }
    }
}
