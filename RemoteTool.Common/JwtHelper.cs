﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace RemoteTool.Common
{
    public static class JwtHelper
    {
        public static string GeneratorToken(string username, JwtTokenConfig config)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.SecretKey));
            // 加密算法
            var credentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
            // 自定义claims
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                new Claim("app","RemoteTool"),
                new Claim(ClaimTypes.Name,username)
            };
            var token = new JwtSecurityToken(
                config.Issuer, // 发行者
                config.Audience, // 使用者
                claims,
                expires: DateTime.Now.AddMinutes(int.Parse(config.Expires)),
                signingCredentials: credentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);

        }
    }


}
