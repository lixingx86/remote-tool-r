﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.Common
{
    public class JwtTokenConfig
    {
        /// <summary>
        /// 服务端密钥 一般16bit 以上
        /// </summary>
        public string SecretKey { get; set; }="1234567890123456";
        public string Issuer { get; set; } = "lixing";
        public string Expires { get; set; } = "60";
        public string Audience { get; set; } = "lixing";
    }
}
