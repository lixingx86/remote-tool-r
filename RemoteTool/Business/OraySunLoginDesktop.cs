﻿using CommonServiceLocator;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using RemoteTool.Business.RemoteManager;
using RemoteTool.Data;
using RemoteTool.Data.Model;
using WindowsInput;
using System.Threading.Tasks;
using HandyControl.Controls;

namespace RemoteTool.Business
{
    /// <summary>
    /// 向日葵桌面远程，13版本后界面有变化
    /// </summary>
    //[RemoteTool(ToolCode = "OSL", Memo = "向日葵桌面远程,最高支持10版，暂不支持11版本", ToolName = "向日葵桌面远程")]
    public class OraySunLoginDesktop : RemoteToolBase
    {
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        private extern static IntPtr FindWindow(string lpClassName, string lpWindowName);
        /// <summary>
        /// 打开远程
        /// </summary>
        /// <param name="info"></param>
        public override Task OpenAsync(RemoteInfo info)
        {
            return OpenAsync(info, info.ToolCode == "OSL");
        }
        /// <summary>
        /// 打开远程
        /// </summary>
        /// <param name="info"></param>
        /// <param name="isDesktop"></param>
        protected Task OpenAsync(RemoteInfo info, Boolean isDesktop)
        {
            //String exe = GetExePath("向日葵");

            var cfgManager = ServiceLocator.Current.GetInstance<ConfigManager>();
            var exe = cfgManager.SysConfig.OraySunExePath;
            if (String.IsNullOrEmpty(exe))
            {
                HandyControl.Controls.MessageBox.Show($"向日葵可执行文件路径为空，请先配置");
                return Task.Delay(100);
            }
            exe = Path.Combine(exe, "SunloginClient.exe");
            if (!File.Exists(exe))
            {
                HandyControl.Controls.MessageBox.Show($"路径{exe}下，向日葵可执行文件不存在，请先配置");
                return Task.Delay(100);
            }
            return Task.Run(() =>
            {
                var versionInfo = FileVersionInfo.GetVersionInfo(exe);
                if (versionInfo.ProductVersion.StartsWith("10"))
                {
                    var process = Process.Start(exe, $"--mod=fastcontrol --fastcode={info.ID} --pwd={info.Password}");
                    process.WaitForExit();
                    Console.WriteLine(process.ExitCode);
                }
                else if (versionInfo.ProductVersion.StartsWith("13") || versionInfo.ProductVersion.StartsWith("15"))
                {
                    InputSimulator input = new InputSimulator();

                    //IntPtr ParenthWnd = new IntPtr(0);
                    //ParenthWnd = FindWindow(null, "向日葵远程控制");
                    ////判断这个窗体是否有效
                    //if (ParenthWnd != IntPtr.Zero)
                    //{
                    //    SetForegroundWindow(ParenthWnd);
                    //    Console.WriteLine($"{DateTime.Now}延时3秒");
                    //    Thread.Sleep(3000);
                    //}
                    //else
                    //{
                       
                    //}
                    var process = Process.Start(exe);
                    Console.WriteLine($"{DateTime.Now}开始等待退出**********************************");
                    process.WaitForExit(2000);
                    var code = process.HasExited ? process.ExitCode.ToString() : "not exited";
                    Console.WriteLine($"{DateTime.Now}退出完成->{code}");

                    input.Keyboard.Sleep(1000);
                    input.Keyboard.TextEntry(info.ID);
                    Console.WriteLine($"{DateTime.Now}开始发送tab键");
                    input.Keyboard.Sleep(200);
                    input.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.TAB);
                    input.Keyboard.Sleep(200);
                    for (int i = 0; i < 8; i++)
                    {
                        input.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.BACK);
                    }
                    input.Keyboard.TextEntry(info.Password);
                    Console.WriteLine($"{DateTime.Now}开始发送enter键");
                    input.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.RETURN);
                }
                else
                {
                    InputSimulator input = new InputSimulator();

                    var process = Process.Start(exe);
                    Console.WriteLine($"{DateTime.Now}开始等待退出**********************************");
                    process.WaitForExit(2000);
                    var code = process.HasExited ? process.ExitCode.ToString() : "not exited";
                    Console.WriteLine($"{DateTime.Now}退出完成->{code}");

                    input.Keyboard.Sleep(1000);
                    input.Keyboard.TextEntry(info.ID);
                    Console.WriteLine($"{DateTime.Now}开始发送tab键");
                    input.Keyboard.Sleep(200);

                    input.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.TAB);
                    input.Keyboard.Sleep(200);
                    input.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.TAB);
                    input.Keyboard.Sleep(200);
                    for (int i = 0; i < 8; i++)
                    {
                        input.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.BACK);
                    }
                    input.Keyboard.TextEntry(info.Password);
                    Console.WriteLine($"{DateTime.Now}开始发送enter键");
                    input.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.RETURN);

                }
            });

        }

    }
}
