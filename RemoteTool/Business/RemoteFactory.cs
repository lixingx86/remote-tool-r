﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemoteTool.Business.RemoteManager;

namespace RemoteTool.Business
{
    internal class RemoteFactory
    {
        public static RemoteToolBase CreateInstance(RemoteType type)
        {
            RemoteToolBase remote = null;
            switch (type)
            {
                case RemoteType.OraySun:
                    remote = new OraySunLoginDesktop();
                    break;
                case RemoteType.ToDesk:
                    remote = new ToDeskDesktop();
                    break;
                default:
                    remote = new ToDeskDesktop();
                    break;
            }
            return remote;
        }
    }
}
