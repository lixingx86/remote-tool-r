﻿using System;
using System.Threading.Tasks;
using RemoteTool.Data.Model;

namespace RemoteTool.Business.RemoteManager
{
    public interface IRemoteTool
    {
        /// <summary>
        /// 打开
        /// </summary>
        /// <param name="info"></param>
        Task OpenAsync(RemoteInfo info);
        /// <summary>
        /// 扩展属性编辑窗口
        /// </summary>
        /// <param name="info"></param>
        void ExtensionPropertySetter(RemoteInfo info);

    }
}
