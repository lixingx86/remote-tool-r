﻿using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using RemoteTool.Business.Tools.Helper;
using RemoteTool.Data.Model;

namespace RemoteTool.Business.RemoteManager
{
    public abstract class RemoteToolBase : IRemoteTool
    {
        public virtual void ExtensionPropertySetter(RemoteInfo info) { }

        public abstract Task OpenAsync(RemoteInfo info);

        /// <summary>
        /// 获取软件路径，先从配置文件中获取，其次从注册表中获取，如果是在注册表中获取到，则把路径保存到配置文件中
        /// </summary>
        /// <param name="SoftwareName"></param>
        /// <returns></returns>
        public String GetExePath(String SoftwareName)
        {
            String exe = null;
            try
            {
                //读取配置里的路径
                exe = ConfigurationManager.AppSettings[SoftwareName];
                LogHelper.WriteLine($"从配置文件中读取【{SoftwareName}】的路径：" + exe);
                if (String.IsNullOrEmpty(exe))
                {
                    // 从注册表读取TeamViewer的安装目录
                    LogHelper.WriteLine($"从注册表中读取【{SoftwareName}】路径");
                    if (RemoteToolHelper.GetSoftWare(SoftwareName, out exe))
                    {
                        // 写入配置
                        var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        config.AppSettings.Settings.Remove(SoftwareName);
                        config.AppSettings.Settings.Add(SoftwareName, exe);
                        config.Save();
                    }
                    LogHelper.WriteLine($"无法从注册表中读取【{SoftwareName}】");
                }
            }
            catch (Exception)
            {

                throw;
            }
            
            return exe;
        }
    }
}
