﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using RemoteTool.Business.RemoteManager;
using RemoteTool.Data.Model;

namespace RemoteTool.Business
{
    /// <summary>
    /// TeamViewer桌面远程
    /// </summary>
    //[RemoteTool(ToolCode = "TVD", Memo = "TeamViewer桌面远程", ToolName = "TeamViewer桌面远程")]
    public class TeamViewerDesktop : RemoteToolBase
    {
        /// <summary>
        /// 打开远程
        /// </summary>
        /// <param name="info"></param>
        public override Task OpenAsync(RemoteInfo info)
        {
           return OpenAsync(info, info.ToolCode == "TVD");
        }
        /// <summary>
        /// 打开远程
        /// </summary>
        /// <param name="info"></param>
        /// <param name="isDesktop"></param>
        protected Task OpenAsync(RemoteInfo info, Boolean isDesktop)
        {
            String exe = GetExePath("TeamViewer");

            if(String.IsNullOrEmpty(exe))
            {
                throw new Exception("工具TeamViewer未找到，请确保正确安装，启动已中止");
            }
            exe = Path.Combine(exe, "TeamViewer.exe");

            return Task.Run(() => {
                Process.Start(exe, $"-i {info.ID} -P {info.Password} {(isDesktop ? "" : " -m fileTransfer")}");
            });
            
            
        }
    }
}
