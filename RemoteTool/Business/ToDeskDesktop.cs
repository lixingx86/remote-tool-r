﻿using CommonServiceLocator;
using System;
using System.Diagnostics;
using System.IO;
using RemoteTool.Business.RemoteManager;
using RemoteTool.Data;
using RemoteTool.Data.Model;
using System.Threading.Tasks;

namespace RemoteTool.Business
{
    /*******
     * 获取帮助:
ToDesk.exe -help
ToDesk.exe -help -control
当前版本号:
ToDesk.exe -version
当前运行状态:
ToDesk.exe -status
获取设备代码:
ToDesk.exe -getid
设置安全密码:
ToDesk.exe -setpasswd password
设置开机启动:
ToDesk.exe -autostart on/off
登陆账户并保存:
ToDesk.exe -user aaaa@aaaa -passwd 123456
退出当前登录账号:
ToDesk.exe -signout
发起控制:
ToDesk.exe -control -id 123456789 -passwd 123456(仅支持设备列表里已解锁设备发起控制)
设置代理并启用:
ToDesk.exe -setproxy -proxyip 192.168.0.2 -port 8080 (没有账号密码可不用输入后面的参数) -proxyuser admin -proxypasswd 123456
启用/禁用代理上网:
ToDesk.exe -proxy on/off
退出运行:
ToDesk.exe -exit
     * ********/

    /// <summary>
    /// TeamViewer桌面远程
    /// </summary>
    //[RemoteTool(ToolCode = "ToDesk", Memo = "", ToolName = "ToDesk")]
    public class ToDeskDesktop : RemoteToolBase
    {
        /// <summary>
        /// 打开远程
        /// </summary>
        /// <param name="info"></param>
        public override Task OpenAsync(RemoteInfo info)
        {
            return OpenAsync(info, info.ToolCode == "ToDesk");
        }
        /// <summary>
        /// 打开远程
        /// </summary>
        /// <param name="info"></param>
        /// <param name="isDesktop"></param>
        protected Task OpenAsync(RemoteInfo info, Boolean isDesktop)
        {
            //String exe = GetExePath("ToDesk");
            var cfgManager = ServiceLocator.Current.GetInstance<ConfigManager>();
            var exe = cfgManager.SysConfig.ToDeskExePath;
            if (String.IsNullOrEmpty(exe))
            {
                HandyControl.Controls.MessageBox.Show($"Todesk可执行文件路径为空，请先配置");
                return Task.Delay(100);
            }
            exe = Path.Combine(exe, "ToDesk.exe");
            if(!File.Exists(exe))
            {
                HandyControl.Controls.MessageBox.Show($"路径{exe}下，Todesk可执行文件不存在，请先配置");
                return Task.Delay(100);
            }
            return Task.Run(() =>
            {
                var process = Process.Start(exe, $"-control -id {info.ID} -passwd {info.Password}");
                process.WaitForExit();
                Console.WriteLine(process.ExitCode);
            });

        }
    }
}
