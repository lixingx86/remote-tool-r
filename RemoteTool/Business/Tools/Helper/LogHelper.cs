﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.Business.Tools.Helper
{
    /// <summary>
    /// 一个简单的日志记录
    /// </summary>
    internal class LogHelper
    {
        static string LogFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");
        static string ErrorLogFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ErrorLog");
        public static bool RecordLog = true;
        public static bool DebugLog = true;
        static LogHelper()
        {
            if (!Directory.Exists(LogFolder))
            {
                Directory.CreateDirectory(LogFolder);
            }
        }
        public static void WriteLog(string msg)
        {
            WriteLine(msg);
        }

        public static void WriteLog(string message, Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]    ") + message);
            sb.AppendLine($"ex.Message->{ex.Message}");
            string fileName = DateTime.Now.ToString("yyyyMMdd") + ".log";
            try
            {
                if (RecordLog)
                {
                    File.AppendAllText(Path.Combine(ErrorLogFolder, fileName), sb.ToString(), Encoding.GetEncoding("GB2312"));
                }
                if (DebugLog)
                {
                    Console.WriteLine(sb.ToString());
                }
            }
            catch
            {
            }
        }



        public static void WriteLine(string message, Action<string> showLog = null)
        {
            string temp = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]    ") + message + "\r\n\r\n";
            string fileName = DateTime.Now.ToString("yyyyMMdd") + ".log";
            try
            {
                if (RecordLog)
                {
                    File.AppendAllText(Path.Combine(LogFolder, fileName), temp, Encoding.GetEncoding("GB2312"));
                }
                if (DebugLog)
                {
                    Console.WriteLine(temp);
                }
                if (showLog != null)
                {
                    showLog(message);
                }
            }
            catch
            {
            }
        }
        public static void WriteLine(string message, string filePath)
        {
            string fileName = DateTime.Now.ToString("yyyyMMdd") + ".txt";
            string temp = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]    ") + message + "\r\n";
            try
            {
                if (File.Exists(filePath))
                {
                    File.AppendAllText(filePath, temp, Encoding.GetEncoding("GB2312"));
                }
                else if (Directory.Exists(filePath))
                {
                    File.AppendAllText(Path.Combine(filePath, fileName), temp, Encoding.GetEncoding("GB2312"));
                }
                else
                {
                    string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filePath);
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    File.AppendAllText(Path.Combine(folder, fileName), temp, Encoding.GetEncoding("GB2312"));
                }
            }
            catch
            {
            }
        }

        public static void WriteLine(string className, string funName, string message)
        {
            WriteLine(string.Format("{0}：{1}\r\n{2}", className, funName, message));
        }
    }
}
