﻿using System;
using System.Runtime.InteropServices;

namespace RemoteTool.Business.Tools
{
    internal class Win32Helper
    {
        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("winmm.dll", EntryPoint = "mciSendString", CharSet = CharSet.Auto)]
        public static extern int MciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);


        private const int WM_KEYDOWN = 0x0100;
        private const int WM_KEYUP = 0x0101;
        private const int VK_RETURN = 0x0D;

        [DllImport("User.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(
        int hWnd,　　　// handle to destination window 
        int Msg,　　　 // message 
        int wParam,　// first message parameter 
        int lParam // second message parameter 
        );


        [DllImport("user32.dll", EntryPoint = "keybd_event")]
        public static extern void keybd_event(

          byte bVk,    //虚拟键值

          byte bScan,// 一般为0

          int dwFlags,  //这里是整数类型  0 为按下，2为释放

          int dwExtraInfo  //这里是整数类型 一般情况下设成为 0

          );

        #region 剪切板用到的一些方法
        [DllImport("User32")]
        public static extern bool OpenClipboard(IntPtr hWndNewOwner);

        [DllImport("User32")]
        public static extern bool CloseClipboard();

        [DllImport("User32")]
        public static extern bool EmptyClipboard();

        [DllImport("User32")]
        public static extern bool IsClipboardFormatAvailable(int format);

        [DllImport("User32")]
        public static extern IntPtr GetClipboardData(int uFormat);

        [DllImport("User32", CharSet = CharSet.Unicode)]
        public static extern IntPtr SetClipboardData(int uFormat, IntPtr hMem);


        /// <summary>
        /// 向剪贴板中添加文本
        /// </summary>
        /// <param name="text">文本</param>
        public static void SetText(string text)
        {
            if (!OpenClipboard(IntPtr.Zero))
            {
                SetText(text);
                return;
            }
            EmptyClipboard();
            SetClipboardData(13, Marshal.StringToHGlobalUni(text));
            CloseClipboard();
        }

        #endregion
    }
}
