﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.Data
{
    /// <summary>
    /// 基础类
    /// </summary>
    public class BindableBase : INotifyPropertyChanged
    {
        /// <summary>
        /// 属性改变
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(String propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// 属性改变
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
