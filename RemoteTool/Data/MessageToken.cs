﻿namespace RemoteTool.Data
{
	public class MessageToken
    {
	    public static readonly string LoadShowContent = nameof(LoadShowContent);

        public static readonly string FullSwitch = nameof(FullSwitch);


        public static readonly string MainWindow = nameof(MainWindow);
        public static readonly string RemoteControl = nameof(RemoteControl);


        public static readonly string WebsitesView = nameof(WebsitesView);

        public static readonly string CommonWindow = nameof(CommonWindow);

        public static readonly string BlurWindow = nameof(BlurWindow);

        public static readonly string CustomNonClientAreaWindow = nameof(CustomNonClientAreaWindow);

        public static readonly string NoNonClientAreaDragableWindow = nameof(NoNonClientAreaDragableWindow);

        public static readonly string QQGroupView = nameof(QQGroupView);

        public static readonly string GlowWindow = nameof(GlowWindow);

        public static readonly string NativeCommonWindow = nameof(NativeCommonWindow);

        public static readonly string NavigationWindow = nameof(NavigationWindow);

        public static readonly string DialogContainer = nameof(DialogContainer);

        public static readonly string LangUpdated = nameof(LangUpdated);

        public static readonly string AddDeviceInfo = nameof(AddDeviceInfo);
        public static readonly string ShowUserInfo = nameof(ShowUserInfo);

    }
}