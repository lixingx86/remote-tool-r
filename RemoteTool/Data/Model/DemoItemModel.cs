﻿namespace RemoteTool.Data
{
    public class DemoItemModel
    {
        public string Name { get; set; } = string.Empty;

        public string TargetCtlName { get; set; } = string.Empty;

        public string ImageName { get; set; } = string.Empty;

        public bool IsNew { get; set; } = true;
    }
}
