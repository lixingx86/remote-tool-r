﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.CommandWpf;
using HandyControl.Controls;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemoteTool.Business;
using RemoteTool.IRepositories;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace RemoteTool.Data.Model
{
    public class DeviceInfo : BindableBase
    {
        [SugarColumn(IsNullable = false, IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; set; }
        /// <summary>
        /// 记录的唯一标识
        /// </summary>
        public string GuidCode { get; set; } = string.Empty;
        private string _deviceSerialNo = string.Empty;
        /// <summary>
        /// 设备序列号
        /// </summary>
        public string DeviceSerialNo { get => _deviceSerialNo; set { _deviceSetting = value; OnPropertyChanged(nameof(DeviceSerialNo)); } }

        private string _projectName = string.Empty;
        private string _deviceName = string.Empty;
        private string _serviceIp = string.Empty;
        private string _serviceInfo = string.Empty;
        private string _remarkInfo = string.Empty;
        private string _deviceSetting = string.Empty;

        /// <summary>
        /// 所属项目名称
        /// </summary>
        public string ProjectName { get => _projectName; set { _projectName = value; OnPropertyChanged(nameof(ProjectName)); } }

        /// <summary>
        /// 设备名称 比如借还机、办证机
        /// </summary>
        public string DeviceName { get => _deviceName; set { _deviceName = value; OnPropertyChanged(nameof(DeviceName)); } }

        #region ToDesk
        private string _toDeskId = string.Empty;
        public string ToDeskId { get => _toDeskId.Replace(" ", ""); set { _toDeskId = value; OnPropertyChanged(nameof(ToDeskId)); } }
        private string _toDeskPsw = string.Empty;
        public string ToDeskPsw { get => _toDeskPsw; set { _toDeskPsw = value; OnPropertyChanged(nameof(ToDeskPsw)); } }
        private int _toDeskCount = 0;
        /// <summary>
        /// 点击计数
        /// </summary>
        public int ToDeskCount { get => _toDeskCount; set { _toDeskCount = value; OnPropertyChanged(nameof(ToDeskCount)); } }
        public void ClearToDesk()
        {
            ToDeskCount = 0;
            ToDeskId = string.Empty;
            ToDeskPsw = string.Empty;
        }
        [SugarColumn(IsIgnore = true)]
        public RelayCommand ConnectToDesk { get; }
        #endregion

        #region OraySun
        private string _oraySunId = string.Empty;
        public string OraySunId { get => _oraySunId.Replace(" ", ""); set { _oraySunId = value; OnPropertyChanged(nameof(OraySunId)); } }
        private string _oraySunPsw = string.Empty;
        public string OraySunPsw { get => _oraySunPsw; set { _oraySunPsw = value; OnPropertyChanged(nameof(OraySunPsw)); } }
        private int _oraySunCount = 0;
        public int OraySunCount { get => _oraySunCount; set { _oraySunCount = value; OnPropertyChanged(nameof(OraySunCount)); } }
        public void ClearOraySun()
        {
            OraySunCount = 0;
            OraySunId = string.Empty;
            OraySunPsw = string.Empty;
        }
        [SugarColumn(IsIgnore = true)]
        public RelayCommand ConnectOraySun { get; }
        #endregion

        public void ClearAll()
        {
            Id = 0;
            ClearToDesk();
            ClearOraySun();
        }

        private DateTime lastTime = DateTime.Now;
        /// <summary>
        /// 最近连接时间
        /// </summary>
        public DateTime LastTime { get => lastTime; set { lastTime = value; OnPropertyChanged(nameof(LastTime)); } }
        /// <summary>
        /// 创建者名字
        /// </summary>
        public string CreatorName { get; set; }
        /// <summary>
        /// 创建者时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime? CreateTime { get; set; }

        public DeviceInfo()
        {
            ConnectToDesk = new RelayCommand(ConnectToDeskHandler);
            ConnectOraySun = new RelayCommand(ConnectOraySunHandler);
        }

        private async void ConnectOraySunHandler()
        {
            if (string.IsNullOrEmpty(OraySunId) || string.IsNullOrEmpty(OraySunPsw))
            {
                Growl.Info("向日葵远程账号或密码为空");
            }
            else
            {
                OraySunCount++;
                LastTime = DateTime.Now;
                var repository = ServiceLocator.Current.GetInstance<IRepository<DeviceInfo>>();
                repository.Update(this);
                var remote = RemoteFactory.CreateInstance(RemoteType.OraySun);
                await remote.OpenAsync(new RemoteInfo() { ID = OraySunId, Password = OraySunPsw });
            }
        }

        private async void ConnectToDeskHandler()
        {
            if (string.IsNullOrEmpty(ToDeskId) || string.IsNullOrEmpty(ToDeskPsw))
            {
                Growl.Info("ToDesk远程账号或密码为空");
            }
            else
            {
                ToDeskCount++;
                LastTime = DateTime.Now;
                var repository = ServiceLocator.Current.GetInstance<IRepository<DeviceInfo>>();
                repository.Update(this);
                var remote = RemoteFactory.CreateInstance(RemoteType.ToDesk);
                await remote.OpenAsync(new RemoteInfo() { ID = ToDeskId, Password = ToDeskPsw });
            }
        }

        /// <summary>
        /// 服务器IP地址，这里一般指中间件服务的IP地址
        /// </summary>
        public string ServiceIp { get => _serviceIp; set { _serviceIp = value; OnPropertyChanged(nameof(ServiceIp)); } }
        /// <summary>
        /// 服务器的其他补充信息，可以描述系统版本等信息
        /// </summary>
        public string ServiceInfo { get => _serviceInfo; set { _serviceInfo = value; OnPropertyChanged(nameof(ServiceInfo)); } }
        /// <summary>
        /// 设备描述信息
        /// </summary>
        public string RemarkInfo { get => _remarkInfo; set { _remarkInfo = value; OnPropertyChanged(nameof(RemarkInfo)); } }
        /// <summary>
        /// 设备的配置信息，可以用json字符串的形式进行保存
        /// </summary>
        public string DeviceSetting { get => _deviceSetting; set { _deviceSetting = value; OnPropertyChanged(nameof(DeviceSetting)); } }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as DeviceInfo;
            if (obj == null || obj.GetType() != typeof(DeviceInfo)) return false;
            return (other?.OraySunId.Replace(" ", "") == this.OraySunId.Replace(" ", "") &&
                other?.ToDeskId.Replace(" ", "") == this.ToDeskId.Replace(" ", ""));
        }
    }
}
