﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.Data.Model
{
    /// <summary>
    /// 关键词信息类，用来统计关键词使用频率，进行相关排序
    /// </summary>
    public class keywordInfo
    {
        public string Keyword { get; set; }
        public int Count { get; set; }
        public DateTime LatestTime { get; set; }

        public override string ToString()
        {
            return Keyword;
        }
    }
}
