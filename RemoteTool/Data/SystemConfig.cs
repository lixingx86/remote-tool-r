﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.Data
{
    public class SystemConfig:ViewModelBase
    {
        //D:\Program Files (x86)\Oray\SunLogin\SunloginClient
        private string _oraySunExePath = @"";
        public string OraySunExePath { get => _oraySunExePath; set { Set(nameof(OraySunExePath), ref _oraySunExePath, value); } }
        //D:\Program Files (x86)\ToDesk
        private string _toDeskExePath = @"";
        public string ToDeskExePath { get => _toDeskExePath; set { Set(nameof(ToDeskExePath), ref _toDeskExePath, value); } }

        public string ServerUrl { get; set; } = @"http://192.168.1.20:5000";
        public List<string> DeviceNames { get; set; } = new List<string> { "文件共享服务器","检索机"};
    }
}
