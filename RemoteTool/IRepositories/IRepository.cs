﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.IRepositories
{
    public interface IRepository<T> 
    { 
        T GetById(int id); 
        int Insert(T entity); 
        int Update(T entity); 
        int Delete(T entity);
        Task<bool> AnyAsycn(Expression<Func<T, bool>> expression);
        Task<List<T>> FindAllAsycn(string keyword = null);
        Task<List<T>> FindAllAsycn(Expression<Func<T,bool>> expression);
        Task<List<T>> FindAllAsycn(string keyword, int pageNumber, int pageSize, RefAsync<int> totalNumber);
        Task<List<T>> FindLatestAsycn(int count);
        Task<List<T>> FindPageLatestAsycn(int pageNumber,int pageSize, RefAsync<int> totalNumber);
    }
}
