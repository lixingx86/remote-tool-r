﻿using HandyControl.Controls;
using HandyControl.Properties.Langs;
using HandyControl.Tools;
using RemoteTool.Data;
using RemoteTool.View;
using RemoteTool.View.Main;
using RemoteTool.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using MessageBox = HandyControl.Controls.MessageBox;

namespace RemoteTool
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow() => InitializeComponent();

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);

            DataContext = ViewModelLocator.Instance.Main;
            NonClientAreaContent = new NoUserContent();
            ControlMain.Content = new MainWindowContent();

            GlobalShortcut.Init(new List<KeyBinding>
            {
                new KeyBinding(ViewModelLocator.Instance.Main.GlobalShortcutInfoCmd, Key.I, ModifierKeys.Control | ModifierKeys.Alt),
                new KeyBinding(ViewModelLocator.Instance.Main.GlobalShortcutWarningCmd, Key.E, ModifierKeys.Control | ModifierKeys.Alt),
                new KeyBinding(ViewModelLocator.Instance.RemoteControl.Loaded, Key.R, ModifierKeys.Control | ModifierKeys.Alt)
            });

            Dialog.SetToken(this, MessageToken.MainWindow);
            WindowAttach.SetIgnoreAltF4(this, true);

            //Messenger.Default.Send(true, MessageToken.FullSwitch);
            //Messenger.Default.Send(AssemblyHelper.CreateInternalInstance($"View.Main.{MessageToken.PracticalDemo}"), MessageToken.LoadShowContent);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (GlobalData.NotifyIconIsShow)
            {
                MessageBox.Info(Lang.Close, Lang.Tip);
                Hide();
                e.Cancel = true;
            }
            else
            {
                base.OnClosing(e);
            }
        }
    }
}
