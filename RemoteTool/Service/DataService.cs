﻿using HandyControl.Data;
using HandyControl.Properties.Langs;
using RemoteTool.Business.Tools.Converter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RemoteTool.Data;
using RemoteTool.Data.Model;

namespace RemoteTool.Service
{
    public class DataService
    {
        private ObservableCollection<string> _projectNames = new ObservableCollection<string>() { "公共项目" };
        public ObservableCollection<string> ProjectNames { get => _projectNames; set { _projectNames = value; } }

        private ObservableCollection<string> _deviceNames = new ObservableCollection<string>();
        public ObservableCollection<string> DeviceNames { get => _deviceNames; set { _deviceNames = value; } }

        private ObservableCollection<keywordInfo> _searchNames = new ObservableCollection<keywordInfo>();
        public ObservableCollection<keywordInfo> SearchNames { get => _searchNames; set { _searchNames = value; } }

        public ConfigManager CfgManager { get; }

        public DataService(ConfigManager cfgManager)
        {
            SearchNames = new ObservableCollection<keywordInfo>()
            {
                new keywordInfo(){ Keyword="hello,world",Count=0,LatestTime=DateTime.Now},
                new keywordInfo(){ Keyword="nihao",Count=0,LatestTime=DateTime.Now},
                new keywordInfo(){ Keyword="china",Count=0,LatestTime=DateTime.Now},
            };
            DeviceNames.Clear();
            CfgManager = cfgManager;
            CfgManager.SysConfig.DeviceNames.ForEach(p => DeviceNames.Add(p));
        }

        internal List<DemoItemModel> GetDemoItems()
        {
            List<DemoItemModel> demoItems = new List<DemoItemModel>();
            demoItems.Add(new DemoItemModel() { Name = "远程控制", TargetCtlName = "RemoteControl", ImageName = "", IsNew = true });
            //demoItems.Add(new DemoItemModel() { Name = "SSH远程", TargetCtlName = "SSHControl", ImageName = "", IsNew = true });
            demoItems.Add(new DemoItemModel() { Name = "其他工具", TargetCtlName = "AdvanceControl", ImageName = "", IsNew = true });
            demoItems.Add(new DemoItemModel() { Name = "高级设置", TargetCtlName = "SettingControl", ImageName = "", IsNew = true });
            return demoItems;
        }

        internal List<DemoDataModel> GetDemoDataList()
        {
            var list = new List<DemoDataModel>();
            for (var i = 1; i <= 20; i++)
            {
                var dataList = new List<DemoDataModel>();
                for (var j = 0; j < 3; j++)
                {
                    dataList.Add(new DemoDataModel
                    {
                        Index = j,
                        IsSelected = j % 2 == 0,
                        Name = $"SubName{j}",
                        Type = (DemoType)j
                    });
                }
                var model = new DemoDataModel
                {
                    Index = i,
                    IsSelected = i % 2 == 0,
                    Name = $"Name{i}",
                    Type = (DemoType)i,
                    DataList = dataList,
                    ImgPath = $"/RemoteTool;component/Resources/Img/Avatar/avatar{i % 6 + 1}.png",
                    Remark = new string(i.ToString()[0], 10)
                };
                list.Add(model);
            }

            return list;
        }

        public List<DemoDataModel> GetDemoDataList(int count)
        {
            var list = new List<DemoDataModel>();
            for (var i = 1; i <= count; i++)
            {
                var index = i % 6 + 1;
                var model = new DemoDataModel
                {
                    Index = i,
                    IsSelected = i % 2 == 0,
                    Name = $"Name{i}",
                    Type = (DemoType)index,
                    ImgPath = $"/RemoteTool;component/Resources/Img/Avatar/avatar{index}.png",
                    Remark = new string(i.ToString()[0], 10)
                };
                list.Add(model);
            }

            return list;
        }

        internal List<string> GetComboBoxDemoDataList()
        {
            var converter = new StringRepeatConverter();
            var list = new List<string>();
            for (var i = 1; i <= 9; i++)
            {
                list.Add($"{converter.Convert(Lang.All, null, i, CultureInfo.CurrentCulture)}{i}");
            }

            return list;
        }

    }
}
