﻿using Newtonsoft.Json.Linq;
using RemoteTool.Data.Model;
using RemoteTool.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.Service
{
    public interface IWebApiService
    {
        Task<ApiResult<string>> GetAccessTokenAsync(string userName, string password);
        Task<ApiResult<UserInfo>> GetUserInfoAsync(string userName);
        Task<ApiResult<DeviceInfo>> AddDeviceInfoAsync(DeviceInfo info);
        Task<ApiResult<DeviceInfo>> DeleteDeviceInfoAsync(string guidCode);
        Task<ApiResult<DeviceInfo>> ModifyDeviceInfoAsync(DeviceInfo info);
        Task<ApiResult<List<DeviceInfo>>> SearchDeviceInfoAsync(string keyword);

        Task<ApiResult<List<DeviceInfo>>> DownloadDeviceInfosAsync();
        Task<ApiResult<string>> UploadDeviceInfosAsync(List<DeviceInfo> deviceInfos);
    }
}
