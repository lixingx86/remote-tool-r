﻿using RemoteTool.Data.Model;
using RemoteTool.ViewModel.Controls;

namespace RemoteTool.Window
{
    public partial class AddDeviceWindow
    {
        public AddDeviceWindow()
        {
            InitializeComponent();
            this.Loaded += AddDeviceWindow_Loaded;
        }

        private void AddDeviceWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                var addDevMode = this.DataContext as AddDeviceWindowViewModel;
                var devInfo = this.Tag as DeviceInfo;
                if (devInfo != null)
                {
                    addDevMode.DevInfo.ProjectName = devInfo.ProjectName;
                    addDevMode.DevInfo.DeviceName = devInfo.DeviceName;
                }
            }
            catch (System.Exception)
            {
            }
        }
    }
}
