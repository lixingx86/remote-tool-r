﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteTool.ViewModel.Controls
{
    public class SSHControlViewModel:ViewModelBase
    {
        public RelayCommand Loaded { get; }
        public SSHControlViewModel()
        {
            Loaded = new RelayCommand(LoadedHandler);
        }

        private void LoadedHandler()
        {
           
        }
    }
}
