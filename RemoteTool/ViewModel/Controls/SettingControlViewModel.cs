﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Threading;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemoteTool.Data;

namespace RemoteTool.ViewModel.Controls
{
    public class SettingControlViewModel:ViewModelBase
    {
        public ConfigManager CfgManager { get; }
        public RelayCommand Loaded { get; }
        public RelayCommand SaveConfig { get; }
        public RelayCommand<string> SearchPath { get; }
        public SettingControlViewModel(ConfigManager cfgManager)
        {
            Loaded = new RelayCommand(LoadedHandler);
            SaveConfig = new RelayCommand(() => { GlobalData.Save();CfgManager.Save(); });
            SearchPath = new RelayCommand<string>(SearchPathHandler);
            CfgManager = cfgManager;
        }

        private static string filePath = @"C:\";
        private void SearchPathHandler(string target)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = filePath;//注意这里写路径时要用d:\\而不是d:\
            ofd.Filter = "exe执行文件|*.exe|全部文件|*.*";
            ofd.RestoreDirectory = false;
            ofd.FilterIndex = 0;
            if (ofd.ShowDialog() == true)
            {
                
                filePath = Path.GetDirectoryName(ofd.FileName);
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    if (target == "ToDesk")
                    {
                        CfgManager.SysConfig.ToDeskExePath = filePath;
                    }
                    else if (target == "OraySun")
                    {
                        CfgManager.SysConfig.OraySunExePath = filePath;
                    }
                    CfgManager.Save();
                });
            }
        }

        private void LoadedHandler()
        {
            
        }
    }
}
