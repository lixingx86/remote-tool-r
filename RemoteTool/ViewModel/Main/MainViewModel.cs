﻿using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using HandyControl.Controls;
using HandyControl.Properties.Langs;
using RemoteTool.Business.Tools;
using RemoteTool.Data;
using RemoteTool.Service;
using RemoteTool.Shared;
using RemoteTool.View;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace RemoteTool.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region 字段
        private bool _isLogin = false;
        public bool IsLogin { get { return _isLogin; } set { _isLogin = value; RaisePropertyChanged(nameof(IsLogin)); } }
        /// <summary>
        /// 内容标题
        /// </summary>
        private object _contentTitle;

        /// <summary>
        ///     子内容
        /// </summary>
        private object _subContent;

        private string _userName = "未登录";
        public string UserName { get { return _userName; } set { _userName = value; RaisePropertyChanged(nameof(UserName)); } }

        private BitmapImage _headPortrait = new BitmapImage(new Uri("pack://application:,,,/Resources/Img/color_tree.jpg", UriKind.RelativeOrAbsolute));
        public BitmapImage HeadPortrait
        {
            get
            {
                return _headPortrait;
            }
            set
            {
                _headPortrait = value;
                RaisePropertyChanged(nameof(HeadPortrait));
            }
        }

        #endregion

        public MainViewModel(DataService dataService)
        {
            Messenger.Default.Register<object>(this, MessageToken.LoadShowContent, obj =>
            {
                if (SubContent is IDisposable disposable)
                {
                    disposable.Dispose();
                }
                SubContent = obj;
            }, true);

            Messenger.Default.Register<object>(this, MessageToken.LangUpdated, obj =>
            {
                if (DemoItemCurrent == null) return;
                ContentTitle = LangProvider.GetLang(DemoItemCurrent.Name);
            });

            Messenger.Default.Register<UserInfo>(this, MessageToken.ShowUserInfo, obj =>
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() => {
                    IsLogin = true;
                    UserName = obj.Name; 
                });
            });


            Task.Run(() =>
            {
                var itemModels = dataService.GetDemoItems();
                foreach (var item in itemModels)
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        DemoItemList.Add(item);
                    }), DispatcherPriority.ApplicationIdle);
                }
            });
        }

        #region 属性

        private DemoItemModel itemCurrent;
        /// <summary>
        /// 当前选中的demo项
        /// </summary>
        public DemoItemModel DemoItemCurrent
        {
            get => itemCurrent;
            set
            {
                if (value != null && value.Name != itemCurrent?.Name)
                { SwitchDemo(value); }
                Set(ref itemCurrent, value);
            }

        }

        /// <summary>
        /// 子内容
        /// </summary>
        public object SubContent
        {
            get => _subContent;
            set => Set(ref _subContent, value);
        }

        /// <summary>
        /// 内容标题
        /// </summary>
        public object ContentTitle
        {
            get => _contentTitle;
            set => Set(ref _contentTitle, value);
        }

        /// <summary>
        /// demo信息
        /// </summary>
        public ObservableCollection<DemoItemModel> DemoItemList { get; set; } = new ObservableCollection<DemoItemModel>();

        #endregion

        #region 命令

        public RelayCommand GlobalShortcutInfoCmd => new Lazy<RelayCommand>(() =>
            new RelayCommand(() => Growl.Info("Global Shortcut Info"))).Value;

        public RelayCommand GlobalShortcutWarningCmd => new Lazy<RelayCommand>(() =>
            new RelayCommand(() => Growl.Warning("Global Shortcut Warning"))).Value;

        #endregion

        #region 方法

        private void SwitchDemo(DemoItemModel item)
        {
            ContentTitle = LangProvider.GetLang(item.Name);
            var obj = AssemblyHelper.ResolveByKey(item.TargetCtlName);
            var ctl = obj ?? AssemblyHelper.CreateInternalInstance($"View.Controls.{item.TargetCtlName}");
            Messenger.Default.Send(ctl, MessageToken.LoadShowContent);
        }

        #endregion
    }
}