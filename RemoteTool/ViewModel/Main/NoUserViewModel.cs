﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using RemoteTool.Business.Tools;
using RemoteTool.Data;
using RemoteTool.ViewModel.Controls;
using System;
using System.Windows;

namespace RemoteTool.ViewModel
{
    public class NoUserViewModel : ViewModelBase
    {
        public RelayCommand LoginCmd => new Lazy<RelayCommand>(() =>
            new RelayCommand(LoginCmdHandler)).Value;

        private void LoginCmdHandler()
        {
            if (AssemblyHelper.CreateInternalInstance($"View.Window.LoginWindow") is System.Windows.Window window)
            {
                window.Owner = Application.Current.MainWindow;
                window.DataContext = new LoginViewModel() { CloseAction = () => { window.Close(); } };
                window.ShowDialog();
            }
        }

        public RelayCommand<string> OpenViewCmd => new Lazy<RelayCommand<string>>(() =>
            new RelayCommand<string>(OpenView)).Value;

        private void OpenView(string viewName)
        {
            Messenger.Default.Send(true, MessageToken.FullSwitch);
            Messenger.Default.Send(AssemblyHelper.CreateInternalInstance($"View.Controls.{viewName}"), MessageToken.LoadShowContent);
        }
    }
}