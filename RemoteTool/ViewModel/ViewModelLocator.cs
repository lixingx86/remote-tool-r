﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Windows;
using RemoteTool.Data;
using RemoteTool.Data.Model;
using RemoteTool.IRepositories;
using RemoteTool.Service;
using RemoteTool.ViewModel.Controls;

namespace RemoteTool.ViewModel
{
    internal class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<ConfigManager>(true);
            SimpleIoc.Default.Register<DataService>();
            SimpleIoc.Default.Register<IRepository<DeviceInfo>, SqliteRepository>(true);
            SimpleIoc.Default.Register<IWebApiService, WebApiService>(true);

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<NoUserViewModel>();
            SimpleIoc.Default.Register<SettingControlViewModel>();
            SimpleIoc.Default.Register<RemoteControlViewModel>();
            SimpleIoc.Default.Register<SSHControlViewModel>();
        }

        public static ViewModelLocator Instance => new Lazy<ViewModelLocator>(() =>
                   Application.Current.TryFindResource("Locator") as ViewModelLocator).Value;

        public ConfigManager CfgManager => ServiceLocator.Current.GetInstance<ConfigManager>();
        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public NoUserViewModel NoUser => ServiceLocator.Current.GetInstance<NoUserViewModel>();
        public RemoteControlViewModel RemoteControl => ServiceLocator.Current.GetInstance<RemoteControlViewModel>();
        public SettingControlViewModel SettingControl => ServiceLocator.Current.GetInstance<SettingControlViewModel>();
        public SSHControlViewModel SSHControl => ServiceLocator.Current.GetInstance<SSHControlViewModel>();

    }
}
