﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace RemoteToolData.Data
{
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "城市")]
        public string? City { get; set; }
        [Display(Name = "头像")]
        public string? Portrait { get; set; }
    }
}
