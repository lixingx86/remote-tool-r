﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RemoteToolData.Data
{
    public class DeviceInfo
    {
        public int Id { get; set; }
        /// <summary>
        /// 记录的唯一标识
        /// </summary>
        public string GuidCode { get; set; } = string.Empty;

        private string _projectName = string.Empty;
        private string _deviceName = string.Empty;
        private string _serviceIp = string.Empty;
        private string _serviceInfo = string.Empty;
        private string _remarkInfo = string.Empty;
        private string _deviceSetting = string.Empty;
        private string _deviceSerialNo= string.Empty;

        /// <summary>
        /// 所属项目名称
        /// </summary>
        public string ProjectName { get => _projectName; set { _projectName = value; } }

        /// <summary>
        /// 设备名称 比如借还机、办证机
        /// </summary>
        public string DeviceName { get => _deviceName; set { _deviceName = value; } }
        /// <summary>
        /// 设备序列号（必须要填，如果没有，建议使用网卡的物理地址）
        /// </summary>
        public string DeviceSerialNo { get => _deviceSerialNo; set { _deviceSerialNo = value; } }

        #region ToDesk
        private string _toDeskId = string.Empty;
        public string ToDeskId { get => _toDeskId; set { _toDeskId = value; } }
        private string _toDeskPsw = string.Empty;
        public string ToDeskPsw { get => _toDeskPsw; set { _toDeskPsw = value; } }
        private int _toDeskCount = 0;
        /// <summary>
        /// 点击计数
        /// </summary>
        public int ToDeskCount { get => _toDeskCount; set { _toDeskCount = value; } }
        #endregion

        #region OraySun
        private string _oraySunId = string.Empty;
        public string OraySunId { get => _oraySunId; set { _oraySunId = value; } }
        private string _oraySunPsw = string.Empty;
        public string OraySunPsw { get => _oraySunPsw; set { _oraySunPsw = value; } }
        private int _oraySunCount = 0;
        public int OraySunCount { get => _oraySunCount; set { _oraySunCount = value; } }
        #endregion

        private DateTime lastTime = DateTime.Now;
        /// <summary>
        /// 最近连接时间
        /// </summary>
        public DateTime LastTime { get => lastTime; set { lastTime = value; } }

        public DeviceInfo()
        {
        }
      
        /// <summary>
        /// 服务器IP地址，这里一般指中间件服务的IP地址
        /// </summary>
        public string ServiceIp { get => _serviceIp; set { _serviceIp = value; } }
        /// <summary>
        /// 服务器的其他补充信息，可以描述系统版本等信息
        /// </summary>
        public string ServiceInfo { get => _serviceInfo; set { _serviceInfo = value; } }
        /// <summary>
        /// 设备描述信息
        /// </summary>
        public string RemarkInfo { get => _remarkInfo; set { _remarkInfo = value; } }
        /// <summary>
        /// 设备的配置信息，可以用json字符串的形式进行保存
        /// </summary>
        public string DeviceSetting { get => _deviceSetting; set { _deviceSetting = value; } }
        /// <summary>
        /// 创建者名字
        /// </summary>
        public string? CreatorName { get; set; }
        /// <summary>
        /// 创建者时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime? CreateTime { get; set; }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            var other = obj as DeviceInfo;
            if (obj == null|| obj.GetType() != typeof(DeviceInfo)) return false;
            return (other?.OraySunId.Replace(" ","")==this.OraySunId.Replace(" ", "")&&
                other?.ToDeskId.Replace(" ", "") == this.ToDeskId.Replace(" ", ""));
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
