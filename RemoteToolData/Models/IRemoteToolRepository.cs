﻿using RemoteTool.Data.DtoModel;
using RemoteTool.Data.Repository;
using RemoteTool.Shared;
using RemoteToolData.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RemoteToolData.Models
{
    public interface IRemoteToolRepository : IBaseService<DeviceInfo>
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        Task<ApiResult<Page<DeviceInfo>>> GetPageUser(PageParm param);

    }
}
