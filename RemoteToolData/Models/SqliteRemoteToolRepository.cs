﻿using Microsoft.Extensions.Logging;
using RemoteTool.Data.DtoModel;
using RemoteTool.Data.Repository;
using RemoteTool.Shared;
using RemoteToolData.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RemoteToolData.Models
{
    public class SqliteRemoteToolRepository : BaseService<DeviceInfo>, IRemoteToolRepository
    {
        private readonly ApplicationDbContext dbContext;

        public SqliteRemoteToolRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            this.dbContext = dbContext;

        }

        public Task<ApiResult<Page<DeviceInfo>>> GetPageUser(PageParm param)
        {
            throw new NotImplementedException();
        }
    }
}
