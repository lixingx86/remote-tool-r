﻿using Microsoft.AspNetCore.Mvc;

namespace RemoteTool.Server.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult NotFound404()
        {
            return View();
        }
    }
}
