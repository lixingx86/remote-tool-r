﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RemoteToolData.Data;
using RemoteTool.Server.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace RemoteTool.Server.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ApplicationDbContext applicationDbContext;
        private readonly IWebHostEnvironment webHostEnvironment;

        public ProfileController(UserManager<ApplicationUser> userManager,ApplicationDbContext applicationDbContext,IWebHostEnvironment webHostEnvironment)
        {
            this.userManager = userManager;
            this.applicationDbContext = applicationDbContext;
            this.webHostEnvironment = webHostEnvironment;
        }
        public IActionResult MyProfile(string userName)
        {
            var profile = new ProfileViewModel();
            var user = applicationDbContext.Users.Where(p => p.UserName == userName).FirstOrDefault();
            if (user != null)
            {
                profile.UserName = user.UserName;
                profile.Email = user.Email;
                //profile.Photo = user.Portrait ?? @"~/img/color_tree.jpg";
                profile.Photo = user.Portrait;
                return View(profile);
            }
            else
            {
                ViewBag.ErrorMsg = $"找不到用户名为{userName}的用户";
                return View("~/Views/Error/Error.cshtml");
            }
        }
        [HttpGet]
        public IActionResult EditProfile(string userName)
        {
            var profile = new EditProfileViewModel();
            var user = applicationDbContext.Users.Where(p => p.UserName == userName).FirstOrDefault();
            if (user != null)
            {
                profile.UserName = user.UserName;
                profile.Email = user.Email;
                return View(profile);
            }
            else
            {
                ViewBag.ErrorMsg = $"找不到用户名为{userName}的用户";
                return View("~/Views/Error/Error.cshtml");
            }
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Modify(EditProfileViewModel model)
        {
            var pmodel = new ProfileViewModel();
            if (ModelState.IsValid)
            {
                var user=applicationDbContext.Users.Where(p => p.UserName == model.UserName).FirstOrDefault();
                if (user != null)
                {
                    user.Email = model.Email;
                    if (model.Photo != null)
                    {
                        var uploadFolder = Path.Combine(webHostEnvironment.WebRootPath, @"img/user");
                        var uniqueFileName = Guid.NewGuid().ToString() + model.Photo.FileName;
                        var filePath = Path.Combine(uploadFolder, uniqueFileName);
                        model.Photo.CopyTo(new FileStream(filePath, FileMode.Create));
                        //foreach (var item in model.Photos)
                        //{
                        //    var uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                        //    uniqueFileName = Guid.NewGuid().ToString() + item.FileName;
                        //    var filePath = Path.Combine(uploadFolder, uniqueFileName);
                        //    item.CopyTo(new FileStream(filePath, FileMode.Create));
                        //}
                        user.Portrait = uniqueFileName;
                    }
                    applicationDbContext.SaveChanges();
                    pmodel.UserName = user.UserName;
                    pmodel.Email = user.Email;
                    pmodel.Photo = user.Portrait;
                }
            }
            return View("MyProfile", pmodel);

        }
    }
}
