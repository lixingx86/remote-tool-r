﻿using Microsoft.AspNetCore.Mvc;

namespace RemoteTool.Server.Controllers
{
    public class SettingsController : Controller
    {
        public IActionResult Index()
        {
            return View("~/Views/Settings.cshtml");
        }
    }
}
