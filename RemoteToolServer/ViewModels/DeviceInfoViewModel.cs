﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace RemoteTool.Server.ViewModels
{
    public class DeviceInfoViewModel
    {
        public string GuidCode { get; set; }=string.Empty;
        #region ToDesk
        private string _toDeskId = string.Empty;
        [Required]
        [Display(Name = "ToDesk ID")]
        public string ToDeskId { get => _toDeskId; set { _toDeskId = value; } }
        private string _toDeskPsw = string.Empty;
        [Required]
        [Display(Name = "ToDesk 密码")]
        public string ToDeskPsw { get => _toDeskPsw; set { _toDeskPsw = value; } }
        private int _toDeskCount = 0;
        /// <summary>
        /// 点击计数
        /// </summary>
        public int ToDeskCount { get => _toDeskCount; set { _toDeskCount = value; } }
        #endregion

        #region OraySun
        private string _oraySunId = string.Empty;
        [Required]
        [Display(Name = "向日葵 ID")]
        public string OraySunId { get => _oraySunId; set { _oraySunId = value; } }
        private string _oraySunPsw = string.Empty;
        [Required]
        [Display(Name = "向日葵 密码")]
        public string OraySunPsw { get => _oraySunPsw; set { _oraySunPsw = value; } }
        private int _oraySunCount = 0;
        public int OraySunCount { get => _oraySunCount; set { _oraySunCount = value; } }
        #endregion

        /// <summary>
        /// 所属项目名称
        /// </summary>
        [Required]
        [Display(Name = "项目名称")]
        public string ProjectName { get; set; }=string.Empty;

        /// <summary>
        /// 设备名称 比如借还机、办证机
        /// </summary>
        [Required]
        [Display(Name = "设备名称")]
        public string DeviceName { get; set; } = string.Empty;
        /// <summary>
        /// 设备序列号（必须要填，如果没有，建议使用网卡的物理地址）
        /// </summary>
        [Display(Name = "设备序列号")]
        public string DeviceSerialNo { get; set; } = string.Empty;
        /// <summary>
        /// 服务器IP地址，这里一般指中间件服务的IP地址
        /// </summary>
        [Display(Name = "服务器IP地址")]
        public string ServiceIp { get; set; } = string.Empty;
        /// <summary>
        /// 服务器的其他补充信息，可以描述系统版本等信息
        /// </summary>
        [Display(Name = "服务器信息")]
        public string ServiceInfo { get; set; } = string.Empty;
        /// <summary>
        /// 设备描述信息
        /// </summary>
        [Display(Name = "设备描述信息")]
        public string RemarkInfo { get; set; } = string.Empty;
        /// <summary>
        /// 设备的配置信息，可以用json字符串的形式进行保存
        /// </summary>
        [Display(Name = "设备配置信息")]
        public string DeviceSetting { get; set; } = string.Empty;
        /// <summary>
        /// 创建者名字
        /// </summary>
        [Display(Name = "创建者名字")]
        public string? CreatorName { get; set; }
        /// <summary>
        /// 创建者时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime? CreateTime { get; set; }
    }
}
