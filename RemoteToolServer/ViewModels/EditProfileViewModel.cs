﻿using Microsoft.AspNetCore.Mvc;
using RemoteTool.Server.CustomerUtil;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace RemoteTool.Server.ViewModels
{
    public class EditProfileViewModel
    {
        [Required(ErrorMessage = "请输入用户名，不能为空")]
        //[DataType(DataType.Text)]
        [Display(Name = "用户名")]
        public string? UserName { get; set; }

        //[Required(ErrorMessage = "请输入邮箱地址，不能为空")]
        //[EmailAddress]
        //[Display(Name = "邮箱地址")]
        //[Remote(action: "IsEmailInUse", controller: "Account")]
        //[ValidEmailDomain(allowedDomain: "qq.com", ErrorMessage = "电子邮件后缀必须是qq.com")]
        [Display(Name = "邮箱地址")]
        [Required(ErrorMessage = "请输入邮箱地址，它不能为空！")]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9_]+\.[a-zA-Z0-9_.]+$", ErrorMessage = "邮箱的格式不正确")]
        public string? Email { get; set; }

        //[Required(ErrorMessage = "请输入密码，不能为空")]
        //[DataType(DataType.Password)]
        //[Display(Name = "密码")]
        //public string? Password { get; set; }

        [Display(Name = "头像")]
        public IFormFile? Photo { get; set; }
    }
}
