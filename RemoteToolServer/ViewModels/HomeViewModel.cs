﻿using RemoteToolData.Data;
using X.PagedList;

namespace RemoteTool.Server.ViewModels
{
    public class HomeViewModel
    {
        public string? OperaMsg { get; set; } = string.Empty;
        public int DeviceCount { get; set; } 
        public int ProjectCount { get; set; }
        public int PageCount { get; set; }
        public int PageNumber { get; set; }

        public IPagedList<DeviceInfo>? DeviceInfos { get; set; }
    }
}
