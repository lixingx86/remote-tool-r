﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RemoteTool.Server.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "请输入邮箱地址，不能为空")]
        [RegularExpression(@"[1-9]\d{7,10}@qq\.com", ErrorMessage ="请输入QQ邮箱地址")]
        [EmailAddress]
        public string? Email { get; set; }
        [Required(ErrorMessage = "请输入密码，不能为空")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,}$", ErrorMessage ="密码不符合要求，请保证带有字母、特殊字符和数字，长度最少6位")]
        [DataType(DataType.Password)]
        public string? Password { get; set; }

        [Display(Name = "记住我")]
        public bool RememberMe { get; set; }


        public string? ReturnUrl { get; set; }

        // AuthenticationScheme 的命名空间是 Microsoft.AspNetCore.Authentication
        public IList<AuthenticationScheme>? ExternalLogins
        {
            get; set;
        }
    }
}
