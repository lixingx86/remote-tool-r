﻿using Microsoft.AspNetCore.Mvc;
using RemoteTool.Server.CustomerUtil;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace RemoteTool.Server.ViewModels
{
    public class ProfileViewModel
    {
        [Required(ErrorMessage = "请输入用户名，不能为空")]
        [DataType(DataType.Text)]
        [Display(Name = "用户名")]
        public string? UserName { get; set; }

        [Required(ErrorMessage = "请输入邮箱地址，不能为空")]
        [EmailAddress]
        [Display(Name = "邮箱地址")]
        [Remote(action: "IsEmailInUse", controller: "Account")]
        [ValidEmailDomain(allowedDomain: "qq.com", ErrorMessage = "电子邮件后缀必须是qq.com")]
        public string? Email { get; set; }

        [Display(Name = "头像")]
        public string? Photo { get; set; }

    }
}
