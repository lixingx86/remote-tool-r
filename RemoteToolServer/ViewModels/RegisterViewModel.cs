﻿using Microsoft.AspNetCore.Mvc;
using RemoteTool.Server.CustomerUtil;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace RemoteTool.Server.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "请输入用户名，不能为空")]
        [DataType(DataType.Text)]
        [Display(Name = "用户名")]
        public string? UserName { get; set; }

        [Required(ErrorMessage = "请输入邮箱地址，不能为空")]
        [EmailAddress]
        [Display(Name = "邮箱地址")]
        [Remote(action: "IsEmailInUse", controller: "Account")]
        [ValidEmailDomain(allowedDomain: "qq.com", ErrorMessage = "电子邮件后缀必须是qq.com")]
        public string? Email { get; set; }


        [Required(ErrorMessage = "请输入密码，不能为空")]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string? Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "确认密码")]
        [Compare("Password", ErrorMessage = "密码与确认密码不一致，请重新输入")]
        public string? ConfirmPassword { get; set; }
    }
}
